<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
<div class="entry-content  destacado pieHeader">
		<div class="container">
		
		</div>
	</div><!-- .entry-content -->
	<div class="container innerServicios">
		<div class="breadcrumb">
			<?php
			if ( function_exists('yoast_breadcrumb') ) {
			  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
			}
			?>
		</div>
			<div class="row">
				<div id="main" class="site-main" role="main">
				<?php
					the_archive_title( '<h1 class="titleBorder page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
				$slug = 	get_queried_object()->slug;;
				
				?>
<div class="container listaServicios">
	
		<div class="row">
		<?$args = array(
		    'post_type' => 'post',
		    'post_status' => 'publish',
		    'category_name' => $slug,
		    'posts_per_page' => 5,
		);
		$arr_posts = new WP_Query( $args );
		 
		if ( $arr_posts->have_posts() ) :
		 
		    while ( $arr_posts->have_posts() ) :
		        $arr_posts->the_post();
		        ?>
		        <div class="col-lg-3 home-list" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		        	 <a href="<?php the_permalink(); ?>">
		            <?php
		            if ( has_post_thumbnail() ) :
		                the_post_thumbnail();
		            endif;
		            ?>
		            <header class="entry-header">
		                <h1 class="entry-title"><?php the_title(); ?></h1>
		            </header>
		            </a>
		           </div>
		      
		        <?php
		    endwhile;
		endif;
		?>
	</div>
	</div>
			</div><!-- #main -->
	
	</section><!-- #primary -->

<?php

get_footer();
