<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
    $enable_vc = get_post_meta(get_the_ID(), '_wpb_vc_js_status', true);
    if(!$enable_vc ) {
    ?>
    <?php } ?>

	<div class="entry-content  destacado gris">
		<div class="container">
			<?php
				the_content();
			?>
		</div>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() && !$enable_vc ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'wp-bootstrap-starter' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
<div class="container listaServicios">
	<h2 class="titleBorder entry-title">
	Servicios
	</h2>
<div class="row">
	
	<?$args = array(
	    'post_type' => 'post',
	    'post_status' => 'publish',
	    'category_name' => 'servicios',
	    'posts_per_page' => 5,
	);
	$arr_posts = new WP_Query( $args );
	 
	if ( $arr_posts->have_posts() ) :
	 
	    while ( $arr_posts->have_posts() ) :
	        $arr_posts->the_post();
	        ?>
	        <div class="col-lg-3 home-list" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	        	 <a href="<?php the_permalink(); ?>">
	            <?php
	            if ( has_post_thumbnail() ) :
	                the_post_thumbnail();
	            endif;
	            ?>
	            <header class="entry-header">
	                <h1 class="entry-title"><?php the_title(); ?></h1>
	            </header>
	            </a>
	           </div>
	      
	        <?php
	    endwhile;
	endif;
	?>
</div>
</div>
<div class="entry-content  destacado naranja">
		<div class="container">
		<p>
			Somos capaces de resolver un problema combinando varias disciplinas
		</p>
		</div>
	</div><!-- .entry-content -->
<div class="container listaServicios">
	<h2 class="titleBorder entry-title">
	Productos
	</h2>
	<div class="row">
	
	<?$args = array(
	    'post_type' => 'post',
	    'post_status' => 'publish',
	    'category_name' => 'productos',
	    'posts_per_page' => 5,
	);
	$arr_posts = new WP_Query( $args );
	 
	if ( $arr_posts->have_posts() ) :
	 
	    while ( $arr_posts->have_posts() ) :
	        $arr_posts->the_post();
	        ?>
	       
	        	<div class="col-lg-4 home-list" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	        	<a href="<?php the_permalink(); ?>">
	            <?php
	            if ( has_post_thumbnail() ) :
	                the_post_thumbnail();
	            endif;
	            ?>
	            <header class="entry-header">
	                <h1 class="entry-title"><?php the_title(); ?></h1>
	            </header>
	          	</a>
	          </div>
	     
	        <?php
	    endwhile;
	endif;
	?>
</div>
</div>
</article><!-- #post-## -->
