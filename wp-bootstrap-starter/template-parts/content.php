<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>
<div class="entry-content  destacado pieHeader">
		<div class="container">
		
		</div>
	</div><!-- .entry-content -->
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container innerServicios">
		<div class="breadcrumb">
<?php
if ( function_exists('yoast_breadcrumb') ) {
  yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
}
?>
		</div>
		<div class="row">
		
			<div class="col-lg-6">
				<header class="entry-header">
					<?php
					if ( is_single() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					endif;
					?>

					<div class="entry-content">
						<?php
				        if ( is_single() ) :
							the_content();
				        else :
				            the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wp-bootstrap-starter' ) );
				        endif;

							wp_link_pages( array(
								'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wp-bootstrap-starter' ),
								'after'  => '</div>',
							) );
						?>
					</div><!-- .entry-content -->
				</header><!-- .entry-header -->
			</div>
			<div class="col-lg-6">
				<div class="post-thumbnail">
					<?php the_post_thumbnail(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="entry-content  destacado naranja">
		<div class="container">
		<p>
			Somos capaces de resolver un problema combinando varias disciplinas
		</p>
		</div>
	</div><!-- .entry-content -->
	<div class="container listaServicios">
		<h2 class="titleBorder entry-title">
		Servicios
		</h2>
		<div class="row">
		<?$args = array(
		    'post_type' => 'post',
		    'post_status' => 'publish',
		    'category_name' => 'servicios',
		    'posts_per_page' => 5,
		);
		$arr_posts = new WP_Query( $args );
		 
		if ( $arr_posts->have_posts() ) :
		 
		    while ( $arr_posts->have_posts() ) :
		        $arr_posts->the_post();
		        ?>
		        <div class="col-lg-3 home-list" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		        	 <a href="<?php the_permalink(); ?>">
		            <?php
		            if ( has_post_thumbnail() ) :
		                the_post_thumbnail();
		            endif;
		            ?>
		            <header class="entry-header">
		                <h1 class="entry-title"><?php the_title(); ?></h1>
		            </header>
		            </a>
		           </div>
		      
		        <?php
		    endwhile;
		endif;
		?>
	</div>
	</div>
</article>	
