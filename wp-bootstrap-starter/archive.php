<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section id="primary">
		<div id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
					the_archive_slug( '<h1 class="page-title">', '</h1>' );
				?>
			</header><!-- .page-header -->

			

		</div><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
