<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
    <?php get_template_part( 'footer-widget' ); ?>
	<footer id="colophon" class="site-footer 
	 <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
		<div class="container">
		<div class="row justify-content-between">
		<div class="col-md-6">
			<div class="logo">
				<?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
		            <a href="<?php echo esc_url( home_url( '/' )); ?>">
		                <img src="<?php echo esc_url(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
		            </a>
		        <?php else : ?>
		            <a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
		        <?php endif; ?>
    		</div>
    	</div>
    	<div class="content-area col-md-6">
			<ul>
				<li><strong>Direcci&oacute;n:</strong><span>añeiros 2566 - Mor&oacute;, Buenos Aires, Argentina:</span> </li>
				<li><strong>Telefono:</strong><span>+54 9 11 3760 0003</span> </li>
				<li><strong>Mail:</strong><span>info@inmpamet.com.ar</span> </li>
				
			</ul>	
        </div>
		<div class="site-info">
            &copy; <?php echo date('Y'); ?> <?php echo '<a href="'.home_url().'">'.get_bloginfo('name').'</a>'; ?>
        </div><!-- close .site-info -->
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>